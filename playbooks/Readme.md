# Playbooks

1. Use all.yml to deploy all roles.
2. Use page.yml to deploy prometheus, alert-manager, grafana and collectd-exporter
3. use efk.yml to deploy elasticsearch, fluentd and kibana.

To deploy individual components, use their respective yml files
